package jp.rk.decopy;

public enum Placement {
    Left,
    UpDown,
    Up,
    LeftRight,
    Brackets,
    Full,
    Right,
    Under
}